<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kost;

use App\Http\Requests;

class FrontendController extends Controller
{
    public function index() {
    	$kosts = Kost::paginate(8);

    	return view('frontend.index', compact('kosts'));
    }

    public function show($slug) {
    	$kost = Kost::whereSlug($slug)->firstOrFail();
    	$folder_foto_cover = $kost->user->name;
        $fasilitas = explode(',', $kost->fasilitas);

    	return view('frontend.show', compact('kost', 'folder_foto_cover', 'fasilitas'));

    	// return dd($kost->fotoKost);
    }

    public function how() {
        return view('frontend.how');
    }
}
