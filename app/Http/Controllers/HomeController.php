<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;
use App\Kost;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function adminDashboard() {
        return view('admin.index');
    }

    public function memberDashboard() {
        $kost = Laratrust::user()->kost->all();

        return view('member.index', compact('kost'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Laratrust::hasRole('admin')) {
            return $this->adminDashboard(); 
        } 
        if (Laratrust::hasRole('member')) {
            return $this->memberDashboard();
        }
    }
}
