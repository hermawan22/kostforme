<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KostStoreRequest;
use App\Http\Requests\KostUpdateRequest;

use App\Http\Requests;
use App\Kost;
use App\FotoKost;
use Laratrust\LaratrustFacade as Laratrust;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class KostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('member.kost.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KostStoreRequest $request)
    {
        //Proses penyimpanan ke database
        $data = $request->all();
        if (isset($data['fasilitas'])) {
            $data['fasilitas'] = implode(',', $request->input('fasilitas'));
        } else {
            $data['fasilitas'] = 'Kosongan';
        }

        //Simpan gambar cover
        if($request->hasFile('foto_cover')) {
            $upload_cover = $data['foto_cover'];

            $extension = $upload_cover->getClientOriginalExtension();
            // membuat nama file random berikut extension
            $filename = md5(time()) . '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/'. Laratrust::user()->name;
            // memindahkan file ke folder public/img
            $image = $upload_cover->move($destinationPath, $filename);

            Image::make($image)
                ->resize(600, 400)
                ->text('Kost For Me (www.kostfor.me)', 300, 350, function($font) {
                    $font->file(public_path('fonts/orange.ttf'));
                    $font->size(36);
                    $font->align('center');
                    $font->valign('bottom');
                })
                ->save();
            
            // $upload_cover->storeAs('public/images/'.Laratrust::user()->name, $filename);

            $data['foto_cover'] = $filename;
        }

        Kost::create($data);
        
        return redirect('/foto-kost/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fotoKost = Kost::findOrFail($id)->fotoKost->all();
        $kost = Kost::findOrFail($id);

        return view('member.kost.show', compact('fotoKost', 'kost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kost = Kost::findOrFail($id);
        return view('member.kost.edit')->with(compact('kost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KostUpdateRequest $request, $id)
    {
        $kost = Kost::findOrFail($id);

        $fasilitas = $request->input('fasilitas');

        if ($fasilitas) {
            $kost->fasilitas = implode(',', $fasilitas);
        } else {
            $kost->fasilitas = 'Kosongan';
        }

        $kost->nama = $request->input('nama');
        $kost->alamat = $request->input('alamat');
        $kost->tipe = $request->input('tipe');
        $kost->harga_bulanan = $request->input('harga_bulanan');
        $kost->harga_tahunan = $request->input('harga_tahunan');
        $kost->keterangan = $request->input('keterangan');
        $kost->nomor_hp = $request->input('nomor_hp');
        
        if ($request->hasFile('foto_cover')) {
            $upload_cover = $request->file('foto_cover');

            $extension = $upload_cover->getClientOriginalExtension();
            // membuat nama file random berikut extension
            $filename = md5(time()) . '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/'. Laratrust::user()->name;

            // hapus foto_cover lama, jika ada
            if ($kost->foto_cover) {
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'img/'. Laratrust::user()->name .'/'. $kost->foto_cover;
                \File::delete($filepath);
            }

            // memindahkan file ke folder public/img
            $image = $upload_cover->move($destinationPath, $filename);

            Image::make($image)
                ->resize(600, 400)
                ->text('Kost For Me (www.kostfor.me)', 300, 350, function($font) {
                    $font->file(public_path('fonts/orange.ttf'));
                    $font->size(36);
                    $font->align('center');
                    $font->valign('bottom');
                })
                ->save();

            // ganti field foto_cover dengan foto_cover yang baru
            $kost->foto_cover = $filename;
        }

        $kost->save();
        
        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Berhasil menyimpan $kost->title"
        ]);

        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kost = Kost::findOrFail($id);

        if($kost->foto_cover) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'img/'. Laratrust::user()->name .'/'. $kost->foto_cover;
            \File::delete($filepath);
        }
        if($kost->fotoKost) {
            foreach($kost->fotoKost as $fotoKost) {
                $path = public_path() . DIRECTORY_SEPARATOR . 'img/'. Laratrust::user()->name .'/'. $fotoKost->foto;
                \File::delete($path);
            }
        }

        $kost->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Kost berhasil dihapus"
        ]);

        return redirect('/home');
    }
}
