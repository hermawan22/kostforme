<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kost;
use App\FotoKost;
use App\Http\Requests;
use Laratrust\LaratrustFacade as Laratrust;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;

class FotoKostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kost = Laratrust::user()->kost->pluck('nama', 'id')->all();
        return view('member.foto_kost.create', compact('kost'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi
        $this->validate($request, [
          'kost_id' => 'required',
          'foto'    => 'required'
        ]);


        //Proses penyimpanan ke database

        $foto = $request->file('foto');
        //Simpan gambar
        foreach($foto as $upload) {
            // membuat nama file random berikut extension
            $filename = $upload->getClientOriginalName() . '.' . $upload->extension();

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/'. Laratrust::user()->name;
            // memindahkan file ke folder public/img
            $image = $upload->move($destinationPath, $filename);

            Image::make($image)
                ->resize(800, 500)
                ->text('Kost For Me (www.kostfor.me)', 400, 450, function($font) {
                    $font->file(public_path('fonts/orange.ttf'));
                    $font->size(36);
                    $font->align('center');
                    $font->valign('bottom');
                })
                ->save();
            
            // $upload->storeAs('public/images/'.Laratrust::user()->name, $filename);
            $simpan = new FotoKost;
            $simpan->kost_id = $request->input('kost_id');
            $simpan->foto = $filename;
            $simpan->save();
            // return dd($upload);
        }

        return redirect('/home');
    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fotoKost = FotoKost::findOrFail($id);

        if($fotoKost->foto) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'img/'. Laratrust::user()->name .'/'. $fotoKost->foto;
            \File::delete($filepath);
        }

        $fotoKost->delete();

        return back();
    }
}
