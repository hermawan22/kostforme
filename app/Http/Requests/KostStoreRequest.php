<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'            => 'required|unique:kosts,nama',
            'foto_cover'      => 'required|max:2048',
            'alamat'          => 'required',
            'tipe'            => 'required',
            'harga_bulanan'   => 'numeric',
            'harga_tahunan'   => 'numeric',
            'nomor_hp'        => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Nama kost harus diisi.',
            'nama.unique'   => 'Nama kost sudah digunakan, silahkan gunakan nama lainnya.',
            'foto_cover.required'    => 'Foto cover harus diisi.',
            'foto_cover.max'    => 'Maksimal ukuran foto adalah 2MB',
            'alamat.required'    => 'Alamat harus diisi.',
            'tipe.required'  => 'Jenis kost harus diisi.',
            'harga_bulanan.required' => 'Harga bulanan harus berupa angka.',
            'harga_tahunan.required' => 'Harga tahunan harus berupa angka.',
            'nomor_hp.numeric'  => 'Nomor HP harus berupa angka.'
        ];
    }
}
