<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Kost;

class KostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'            => 'required|unique:kosts,nama,'.$this->route('kost'),
            'harga_bulanan'   => 'numeric',
            'harga_tahunan'   => 'numeric'
        ];
    }
}
