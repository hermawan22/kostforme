<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    // Untuk mengubah nilai menjadi true/false bukannya 1/0
    protected $casts = ['is_verified' => 'boolean'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function kost() {
      return $this->hasMany('\App\Kost');
    }

    public function generateVerificationToken()
    {
      $token = $this->verification_token;
        if (!$token) {
            $token = str_random(40);
            $this->verification_token = $token;
            $this->save();
        }
        return $token;
    }


    public function sendVerification()
    {
        $token = $this->generateVerificationToken();
        $user = $this;

        // Cara mengirim email di laravel 5.2 kebawah, karan laravel 5.3 sudah menggunakan Mail::to();
        Mail::send('emails.verification', compact('user', 'token'), function ($m) use ($user){
          $m->to($user->email, $user->name)->subject('Verifikasi Akun KostForMe');
        });
    }

    public function verify() {
        $this->is_verified = 1; 
        $this->verification_token = null; 
        $this->save();
    }

}
