<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sluggable;

class Kost extends Model
{
    use Sluggable;

    protected $fillable = [
        'user_id',
    	'slug',
    	'nama',
    	'foto_cover',
    	'alamat',
    	'fasilitas',
    	'tipe',
    	'harga_bulanan',
        'harga_tahunan',
        'keterangan',
        'nomor_hp'
    ];

    public function setNamaAttribute($nama)
    {
        $this->attributes['nama'] = $nama;

        $this->attributes['slug'] = $this->slug($nama, '');
    }

    public function fotoKost() {
    	return $this->hasMany('\App\FotoKost');
    }

    public function user() {
        return $this->belongsTo('\App\User');
    }
}
