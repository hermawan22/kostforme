<?php

namespace App\Traits;

trait Sluggable {
    public function slug($nama, $unique)
    {
        $slug = str_slug($nama. '-'. $unique);

        // if(static::whereSlug($slug)->exists())
        // {
        //     return $this->slug($nama, $unique + 1);
        // }

        return $this->attributes['slug'] = $slug;
    }
}