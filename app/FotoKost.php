<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoKost extends Model
{
    protected $fillable = [
    	'foto',
    	'kost_id'
    ];

    public function kost() {
    	return $this->belongsTo('\App\Kost');
    }
}
