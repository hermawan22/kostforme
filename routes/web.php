<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Mail\TestMail;

// Routing untuk halaman awal
Route::get('/', 'FrontendController@index');
Route::get('/show-kost/{slug}', 'FrontendController@show');
Route::get('how-to', 'FrontendController@how');

// Routing khusus untuk admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
});

// Routing khusus untuk member
Route::group(['middleware' => 'auth', 'role:member'], function() {
	Route::resource('/kost', 'KostController');
	Route::resource('/foto-kost', 'FotoKostController');
});

// Halaman yang diredirect pertama setelah login / register
Route::get('/home', 'HomeController@index');
Auth::routes();
Route::get('auth/verify/{token}', 'Auth\RegisterController@verify');
Route::get('auth/send-verification', 'Auth\RegisterController@sendVerification');

// Test email
// Route::get('/email', function() {
// 	Mail::to('hermawan.nino@gmail.com')->send(new TestMail);
// });
