@extends('member.layout')

@section('content')
	<div class="container content-body">

		<ol class="breadcrumb">
		  <li><a href="{!! url('/home') !!}">User Page</a></li>
		  <li><a href="#">Create</a></li>
		</ol>

		<div class="col-md-8">

			<h3 class="user-kost">Tambah Kost</h3>

			{!! Form::open(['action' => 'KostController@store', 'method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
				@include('member.kost._form')
			{!! Form::close() !!}
		</div>
		<div class="col-md-4">
		<ul>
			<b>Catatan : </b><br>
			<li>Jika anda telah menekan tombol <b>Simpan & Selanjutnya</b>, maka untuk mengeditnya silahkan ke halaman <a href="/home">awal</a> -> Pilih <b>lihat detail</b> -> Kemudian <b>Edit</b> 
			</li>
			<li>
				Untuk harga, hanya menggunakan angka. Tidak dengan Rp, tanda titik, atau tanda koma.
			</li>
		</ul>
		</div>
	</div>
@endsection
