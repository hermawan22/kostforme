@extends('member.layout')

@section('content')
	<div class="container content-body">
		<ol class="breadcrumb">
		  <li><a href="{!! url('/home') !!}">User Page</a></li>
		  <li><a href="#">Detail Kost</a></li>
		</ol>

		<div class="row kost">
			<div class="panel panel-default">
			  <div class="panel-heading panel-heading-kost-show">
			  	Detail info Kost
			  	<a type="button" href="{!! url('/kost/'.$kost->id.'/edit') !!}" class="btn btn-success button-heading-kost-show"> Edit Info Kost</a>
			  </div>
			  <div class="panel-body">
			    <table class="table">
			    	<tr>
			    		<td><b>Nama Kost</b></td>
			    		<td>{!! $kost->nama !!}</td>
			    	</tr>
			    	<tr>
			    		<td><b>Nomor HP (yang bisa dihubungi)</b></td>
			    		<td>{!! $kost->nomor_hp !!}</td>
			    	</tr>
			    	<tr>
			    		<td><b>Foto Cover Kost</b></td>
			    		<td>
			    			<img src="{!! asset('img/'.Laratrust::user()->name.'/'.$kost->foto_cover) !!}" class="img-responsive img-thumbnail"></center>
			    		</td>
			    	</tr>
			    	<tr>
			    		<td><b>Alamat</b></td>
			    		<td>{!! $kost->alamat !!}</td>
			    	</tr>
			    	<tr>
			    		<td><b>Fasilitas</b></td>
			    		<td>{!! $kost->fasilitas !!}</td>
			    	</tr>
			    	<tr>
			    		<td><b>Jenis Kost</b></td>
			    		<td>{!! $kost->tipe !!}</td>
			    	</tr>
			    	<tr>
			    		<td><b>Harga Kost Perbulan</b></td>
			    		<td> Rp.{!! $kost->harga_bulanan !!}</td>
			    	</tr>
			    	<tr>
			    		<td><b>Harga Kost Pertahun</b></td>
			    		<td> Rp.{!! $kost->harga_tahunan !!}</td>
			    	</tr>
			    	<tr>
			    		<td><b>Keterangan Tambahan</b></td>
			    		<td>{!! $kost->keterangan !!}</td>
			    	</tr>
				</table>
			  </div>
			</div>
			<div class="panel panel-default">
			  <div class="panel-heading panel-heading-kost-show">
			  	Foto Kost
			  	<a type="button" href="/foto-kost/create" class="btn btn-success button-heading-kost-show"> + Tambah Foto Kost</a>
			  </div>
			  <div class="panel-body">
			  	
				@foreach($fotoKost as $kost)
					<div class="col-md-3">
						<div class="panel panel-default">
						  <div class="panel-body">
							<center><img src="{!! asset('img/'.Laratrust::user()->name.'/'.$kost->foto) !!}" class="img-responsive img-thumbnail"></center>
						  </div>
						  <div class="panel-footer" style="text-align: center;">
						  	{!! Form::open(['method'=>'DELETE' ,'action' => ['FotoKostController@destroy', $kost->id]]) !!}
                            	{!! Form::submit('Hapus', array('class' => 'btn btn-danger')) !!}
                            {!! Form::close() !!}
						  </div>
						</div>
					</div>
		    	@endforeach
			  </div>
			</div>
	    </div>
	</div>
@endsection