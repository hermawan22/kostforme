@extends('member.layout')

@section('content')
	<div class="container content-body">

		<ol class="breadcrumb">
		  <li><a href="{!! url('/home') !!}">User Page</a></li>
		  <li><a href="#">Edit Kost</a></li>
		</ol>

		<div class="col-md-8">

			<h3 class="user-kost">Edit Kost</h3>

			{!! Form::model($kost, ['action' => ['KostController@update', $kost->id], 'method' => 'patch', 'files'=>'true', 'class'=>'form-horizontal']) !!}
				@include('member.kost._form')
			{!! Form::close() !!}
		</div>
		<div class="col-md-4">
		<ul>
			<b>Catatan : </b><br>
			<li>
				Untuk harga, hanya menggunakan angka. Tidak dengan Rp, tanda titik, atau tanda koma.
			</li>
		</ul>
		</div>
	</div>
@endsection