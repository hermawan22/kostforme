{!! Form::hidden('user_id', Laratrust::user()->id) !!}

<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
  {!! Form::label('nama', 'Nama Kost', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8">
    {!! Form::text('nama', null, ['class'=>'form-control']) !!}
    {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('nomor_hp') ? ' has-error' : '' }}">
  {!! Form::label('nomor_hp', 'Nomor HP (yang bisa dihubungi)', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8">
    {!! Form::text('nomor_hp', null, ['class'=>'form-control']) !!}
    {!! $errors->first('nomor_hp', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('foto_cover') ? ' has-error' : '' }}">
  {!! Form::label('foto_cover', 'Foto Cover Kost (satu file)', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8">
    {!! Form::file('foto_cover') !!}
    {!! $errors->first('foto_cover', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
  {!! Form::label('alamat', 'Alamat Kost', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8">
    {!! Form::textarea('alamat', null, ['class'=>'form-control']) !!}
    {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('fasilitas') ? ' has-error' : '' }}">
  {!! Form::label('fasilitas', 'Fasilitas Kost', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8 checkbox-fasilitas">
    {!! Form::checkbox('fasilitas[]', 'TV') !!} TV
    {!! Form::checkbox('fasilitas[]', 'AC') !!} AC
    {!! Form::checkbox('fasilitas[]', 'Kasur') !!} Kasur
    {!! Form::checkbox('fasilitas[]', 'Lemari') !!} Lemari
    {!! Form::checkbox('fasilitas[]', 'Meja') !!} Meja
    {!! Form::checkbox('fasilitas[]', 'Kamar mandi di dalam') !!} Kamar mandi di dalam
    {!! Form::checkbox('fasilitas[]', 'Kamar mandi di luar') !!} Kamar mandi luar
    {!! Form::checkbox('fasilitas[]', 'Internet') !!} Internet
    {!! Form::checkbox('fasilitas[]', 'Kipas angin') !!} Kipas angin
    {!! Form::checkbox('fasilitas[]', 'Dapur di kamar') !!} Dapur di kamar
    {!! Form::checkbox('fasilitas[]', 'Dapur bersama') !!} Dapur bersama
  </div>
</div>

<div class="form-group {!! $errors->has('tipe') ? 'has-error' : '' !!}">
{!! Form::label('tipe', 'Jenis Kost', ['class'=>'col-md-4 control-label']) !!} <div class="col-md-8">
{!! Form::select('tipe', ['Campur'=>'Campur', 'Pria'=>'Pria', 'Wanita'=>'Wanita'], null, [
  'class'=>'js-selectize',
  'placeholder' => 'Pilih Tipe Kost']) !!}
    {!! $errors->first('tipe', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('harga_bulanan') ? ' has-error' : '' }}">
  {!! Form::label('harga_bulanan', 'Harga Perbulan (Jika bayar perbulan)', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8">
    {!! Form::text('harga_bulanan', null, ['class'=>'form-control']) !!}
    {!! $errors->first('harga_bulanan', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('harga_tahunan') ? ' has-error' : '' }}">
  {!! Form::label('harga_tahunan', 'Harga Pertahun (Jika bayar pertahun)', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8">
    {!! Form::text('harga_tahunan', null, ['class'=>'form-control']) !!}
    {!! $errors->first('harga_tahunan', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
  {!! Form::label('keterangan', 'Keterangan Tambahan (Jika ada)', ['class'=>'col-md-4 control-label']) !!} 
  <div class="col-md-8">
    {!! Form::textarea('keterangan', null, ['class'=>'form-control']) !!}
    {!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group" style="text-align: right;">
  <div class="col-md-12">
    {!! Form::submit('Simpan & Selanjutanya ->', ['class'=>'btn btn-success']) !!}
  </div>
</div>