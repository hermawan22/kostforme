@extends('member.layout')

@section('content')
	<div class=" container version-title">
		Selamat datang, {!! Laratrust::user()->name !!}!
	</div>

	<div class="container content-body">
		<a type="button" href="/kost/create" class="btn btn-success"> + Tambah Kost</a>
		<!-- <a type="button" href="/foto-kost/create" class="btn btn-primary"> + Tambah Foto Kost</a> -->

		<div class="row kost">
			@foreach($kost as $kost)
	    	<h3 class="heading-kost"> </h3>
	    	<div class="col-lg-4">
	    		<div class="panel panel-default">
				  <div class="panel-heading">{!! $kost->nama !!}</div>
				  <div class="panel-body">
				    <center><img src="{!! asset('img/'.Laratrust::user()->name.'/'.$kost->foto_cover) !!}" class="img-responsive img-thumbnail"></center>
				  </div>
				  <div class="panel-footer">
				  	<a class="btn btn-success" style="float:left; margin-right: 10px;" href="{!! url('kost', $kost->id) !!}" role="button">Lihat Detail</a>	
				  	{!! Form::open(['method'=>'DELETE' ,'action' => ['KostController@destroy', $kost->id]]) !!}
                    	{!! Form::submit('Hapus', array('class' => 'btn btn-danger')) !!}
                    {!! Form::close() !!}
				  </div>
				</div>
	    	</div>
	    	@endforeach
	    </div>
	</div>
@endsection