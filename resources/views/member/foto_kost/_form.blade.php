<div class="form-group {!! $errors->has('kost_id') ? 'has-error' : '' !!}">
{!! Form::label('kost_id', 'Kost', ['class'=>'col-md-3 control-label']) !!} <div class="col-md-9">
{!! Form::select('kost_id', $kost, null, [
  'class'=>'js-selectize',
  'placeholder' => 'Pilih Kost']) !!}
    {!! $errors->first('kost_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
  {!! Form::label('foto', 'Foto Kost (Multiple file)', ['class'=>'col-md-3 control-label']) !!} 
  <div class="col-md-9">
    {!! Form::file('foto[]', ['multiple'=>true]) !!}
    {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group" style="text-align: right;">
  <div class="col-md-12">
    {!! Form::submit('Finish', ['class'=>'btn btn-success']) !!}
  </div>
</div>