@extends('member.layout')

@section('content')
	<div class="container content-body">
		<ol class="breadcrumb">
		  <li><a href="{!! url('/home') !!}">User Page</a></li>
		  <li><a href="#">Tambah Foto Kost</a></li>
		</ol>


		<div class="col-md-8">
			<h3 class="user-kost">Tambah Foto Kost</h3>

			{!! Form::open(['action' => 'FotoKostController@store', 'method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
				@include('member.foto_kost._form')
			{!! Form::close() !!}
			</div>
		<div class="col-md-4">
			<ul>
				<b>Catatan : </b><br>
				<li>Pilih kost, yang akan ditambahkan fotonya.</li>
				<li>Pilih beberapa foto untuk di upload, karna proses penguploadan mendukung beberapa foto sekaligus untuk di upload.</li>
			</ul>
		</div>
	</div>
@endsection
