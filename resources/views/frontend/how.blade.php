@extends('frontend.layout')

@section('content')
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<div class="row breadcumb-section">
				<ol class="breadcrumb">
				  <li><a class="fa fa-home fa-lg" href="{{url('/')}}"></a></li>
				  <li><a href="#">Cara promosi kost</a></li>
				</ol>
			</div>

			<div class="cara-promosi">
				<h2 style="text-align: center">Cara promosi kost :</h2><br>
				<ol>
					<li>"Klik menu <b>"Prmosi Kost!"</b> yang berda di pojok kanan atas halaman web.</li>
					<li>Silahkan melakukan pendaftaran dengan mengklik tombol <b>"Daftar"</b> dan isi kolom yang tersedia (dengan email yang benar dan aktif)</li>
					<li>Setelah itu lakukan aktivitas dengan mengklik "url verifikasi" yang telah dikirim ke email anda.</li>
					<li>Setelah terverifikasi, maka anda akan berada di halaman home dari user page.</li>
					<li>Kemudian klik tombok <b>"+ Tambah Kost"</b></li>
					<li>Isi kolom - kolom yang telah disediakan sesuai dengan data kost anda dengan benar, kemudian klik tombol <b>"Simpan & Selanjutnya"</b></li>
					<li>Setelah itu anda akan ke halaman untuk menambahkan foto kost, silahkan pilih kost yang ingin ditambahkan fotonya dan pilih beberapa foto kost anda, kemudia klik tombol <b>"Finish"</b></li>
					<li>Kost anda sudah terbuat dan terlihat di halaman utama kostfor.me</li>
				</ol>
			</div>
		</div>
	</div>
@endsection