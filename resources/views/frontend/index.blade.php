@extends('frontend.layout')

@section('content')
<div class="jumbotron">
    <div class="container">
        <div class="col-md-12 text-center">
            <h1>Kost For Me</h1>
            <p class="lead">Tempat untuk mencari kost dan mempromosikan kost kamu!</p>
            <ul class="list-unstyled">
                <li>Beta version</li>
            </ul>

            <a class="btn btn-success" href="{{url('how-to')}}" role="button">Cara Promosi Kost Anda?</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row kost">
    	<h3 class="heading-kost"> Kost di Nusa Tenggara Barat </h3>

    	@foreach($kosts as $kost)
	    	<a href="{!! url('show-kost', $kost->slug) !!}" class="col-lg-4 panel-kost">
	    		<div class="panel panel-default">
				  <div class="panel-heading">{!! $kost->nama !!}</div>
				  <div class="panel-body">
				    <img src="{!! asset('img/'.$kost->user->name.'/'.$kost->foto_cover) !!}" class="img-responsive" alt="Responsive image">
				  </div>
				  <div class="panel-footer" style="text-align: center">
            @if($kost->harga_bulanan)
              <button class="btn btn-default" type="submit">Rp.{!! $kost->harga_bulanan !!} / Bulan</button>
            @endif
            @if($kost->harga_tahunan)
              <button class="btn btn-default" type="submit">Rp.{!! $kost->harga_tahunan !!} / Tahun</button>
            @endif
            </div>
				</div>
	    	</a>
    	@endforeach
    </div>

    {{ $kosts->links() }}
</div>
@endsection