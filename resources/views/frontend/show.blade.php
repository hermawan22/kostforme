@extends('frontend.layout')

@section('content')
<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<div class="row breadcumb-section">
			<ol class="breadcrumb">
			  <li><a class="fa fa-home fa-lg" href="{{url('/')}}"></a></li>
			  <li><a href="#">{!! $kost->nama !!}</a></li>
			</ol>
		</div>

		<h4> {!! $kost->nama !!} </h4>
	    <div class="row kost">
	    	<div class="rslides_container">
		    	<ul class="rslides" id="slider1">
	                @foreach($kost->fotoKost as $key => $value)
	                    <li>
	                        <center><img itemprop="image" src="{!! asset('img/'.$folder_foto_cover.'/'.$value->foto) !!}" class="img-responsive img-thumbnail img-rounded"></center>
	                    </li>
	                @endforeach
	            </ul>
            </div>
	    	
	    	<div class="panel panel-success">
			  <div class="panel-heading heading-info-kost">Info Kost</div>
			  <div class="panel-body body-info-kost">
			  	<table class="table">
				  <tr>
				  	<td><b>Alamat Kost</b></td>
				  	<td>: {!! $kost->alamat !!}</td>
				  </tr>
				  <tr>
				  	<td><b>Jenis Kost</b></td>
				  	<td>: {!! $kost->tipe !!}</td>
				  </tr>
				  <tr>
				  	<td><b>Harga Perbulan</b></td>
				  	<td>
				  		@if($kost->harga_bulanan)
		    			: Rp.{!! $kost->harga_bulanan !!} <br>
			    		@else
			    		  	: - <br>
			    		@endif
				  	</td>
				  </tr>
				  <tr>
				  	<td><b>Harga Pertahun</b></td>
				  	<td>
				  		@if($kost->harga_tahunan)
		    			: Rp.{!! $kost->harga_tahunan !!} <br>
			    		@else
			    		  	: - <br>
			    		@endif
				  	</td>
				  </tr>
				  <tr>
				  	<td><b>Kontak</b></td>
				  	<td>: {!! $kost->nomor_hp!!}</td>
				  </tr>
				</table>
			  </div>
			</div>

			<div class="panel panel-success">
			  <div class="panel-heading heading-info-kost">Fasilitas</div>
			  <div class="panel-body body-info-kost">
			  	@foreach($fasilitas as $fasilitas) 
			  		<i class="fa fa-check-square" aria-hidden="true"></i>

			  		<li class="list-fasilitas">
			  			{!! $fasilitas !!}
			  		</li>
			  	@endforeach
			  </div>
			</div>

			<div class="panel panel-success">
			  <div class="panel-heading heading-info-kost">Keterangan Tambahan</div>
			  <div class="panel-body body-info-kost">
			  {!! $kost->keterangan !!}
			  </div>
			</div>
	    </div>
	</div>
</div>
@endsection