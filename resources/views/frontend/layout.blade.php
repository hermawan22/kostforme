<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Kost For Me</title>

        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="/css/style.css" rel="stylesheet" type="text/css">
        <link href="/css/slider.css" rel="stylesheet" type="text/css">
        <link href="/css/animate.css" rel="stylesheet" type="text/css">

        <!-- Facebook crawler -->
        <meta property="og:url"                content="http://www.kostfor.me" />
        <meta property="og:type"               content="web application" />
        <meta property="og:title"              content="Tempat Mencari Kost Dan Mempromosikan Kost" />
        <meta property="og:description"        content="kostfor.me solusi untuk mencari dan mempromosikan kost anda" />
        <meta property="og:image"              content="{{asset('img/facebook-image.png')}}" />

        <!-- Responsive slider -->
        <link href="/css/responsiveslides.css" rel="stylesheet" type="text/css">

        <!-- Google Analytics -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-84863060-1', 'auto');
          ga('send', 'pageview');

        </script>
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{!! url('/') !!}">kostfor.me</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <!-- <li>
                            <a href="#">About</a>
                        </li> -->
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" data-toggle="modal" class="animated infinite pulse promosi-kost-text" data-target="#myModal">Promosikan Kostmu!</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        @yield('content')
        <!-- /.Page Content -->

        <!-- Modal register/login -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Daftar / Masuk </h4>
                </div>
                <div class="modal-body login-register">
                    <a type="button" href="{{ url('/login') }}" class="btn btn-lg btn-primary"> Masuk </a>
                    <a type="button" href="{{ url('/register') }}" class="btn btn-lg btn-success"> Daftar </a> <br/><br/>
                    *Untuk mempromosikan kost kamu, silahkan untuk melakukan <b>pendaftaran</b> terlebih dahulu,
                    atau <b>login</b> jika telah melakukan pendaftaran sebelumnya <br>
                </div>
            </div>
          </div>
        </div>

        <div class="footer">
            <div class="container">
                kostfor.me&copy;2016
            </div>
        </div>

        <!-- Javascript -->
        <script src="/js/jquery.min.js" type="text/javascript"></script>
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/js/responsiveslides.min.js" type="text/javascript"></script>
        <script>
            $("#slider1").responsiveSlides({
              auto: true,             // Boolean: Animate automatically, true or false
              speed: 500,            // Integer: Speed of the transition, in milliseconds
              timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
              pager: true,           // Boolean: Show pager, true or false
              nav: false,             // Boolean: Show navigation, true or false
              random: false,          // Boolean: Randomize the order of the slides, true or false
              pause: false,           // Boolean: Pause on hover, true or false
              pauseControls: true,    // Boolean: Pause when hovering controls, true or false
              prevText: "Previous",   // String: Text for the "previous" button
              nextText: "Next",       // String: Text for the "next" button
              maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
              navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
              manualControls: "",     // Selector: Declare custom pager navigation
              namespace: "centered-btns",   // String: Change the default namespace used
              before: function(){},   // Function: Before callback
              after: function(){}     // Function: After callback
            });
        </script>
    </body>
</html>
